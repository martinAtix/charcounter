package com.nicopaez;


import org.junit.Assert;
import org.junit.Test;


public class StringTest {

    @Test
    public void containsOkMiddleTrue() {
        String value = "abc1def";
        String valueToFind = "1";
        value.contains(valueToFind);

        Assert.assertTrue(value.contains(valueToFind));
    }

    @Test
    public void containsOkInitialTrue() {
        String value = "abc1def";
        String valueToFind = "a";
        value.contains(valueToFind);

        Assert.assertTrue(value.contains(valueToFind));
    }

    @Test
    public void containsOkEndTrue() {
        String value = "abc1def";
        String valueToFind = "f";
        value.contains(valueToFind);

        Assert.assertTrue(value.contains(valueToFind));
    }

    @Test
    public void containsOkFalse() {
        String value = "abc1def";
        String valueToFind = "2";
        value.contains(valueToFind);

        Assert.assertFalse(value.contains(valueToFind));
    }

    @Test(expected = NullPointerException.class)
    public void containsSetNullThrowNullPointerException() {
        String value = "abc1def";
        String valueToFind = null;

        value.contains(valueToFind);

        Assert.assertFalse(value.contains(valueToFind));
    }
}

