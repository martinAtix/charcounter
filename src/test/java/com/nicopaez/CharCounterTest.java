package com.nicopaez;


import org.junit.Test;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CharCounterTest {

    @Test
    public void whenInputIsEmptyResultIsEmpty() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("");
        assert(result.isEmpty());
    }

    @Test
    public void whenInputNullResultIsEmpty() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll(null);
        assert(result.isEmpty());
    }

    @Test
    public void whenInputOnlyOneCharGetOneResult() {
        CharCounter counter = new CharCounter();

        String value = "a";
        Character valueToEvaluate = 'a';

        HashMap<Character, Integer> result =  counter.countAll(value);

        assertThat("Contains a one time",result.get(valueToEvaluate),is(1));
        assertThat("Size is one",result.size(),is(1));
    }

    @Test
    public void whenInputThreeCharsAndSpaceGetOneFourCharsResult() {
        CharCounter counter = new CharCounter();

        String value = "a ab abc";

        HashMap<Character, Integer> result =  counter.countAll(value);

        assertThat("Contains a Three times",result.get('a'),is(3));
        assertThat("Contains b two times",result.get('b'),is(2));
        assertThat("Contains c one time",result.get('c'),is(1));
        assertThat("Contains ' ' two times",result.get('b'),is(2));
        assertThat("Size is Four",result.size(),is(4));
    }

    @Test
    public void whenLookForStringValuetGetZeroResult() {
        CharCounter counter = new CharCounter();

        String value = "a";

        HashMap<Character, Integer> result =  counter.countAll(value);

        assertThat("Contains a one time",result.get(value),is(0));
        assertThat("Size is one",result.size(),is(1));
    }
}
